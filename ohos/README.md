# map_launcher_ohos

Flutter插件，用于根据经纬度信息打开已安装的地图应用，起掉定位和导航功能

## Usage

```pod
dependencies:
    map_launcher: any
    map_launcher_ohos: any
```

### Example

```pod
import 'package:map_launcher_ohos/map_launcher.dart';

static Future<List<AvailableMap>> get installedMaps async {
    final maps = await _channel.invokeMethod('getInstalledMaps');
    return List<AvailableMap>.from(
      maps.map((map) => AvailableMap.fromJson(map)),
    );
  }

Future<dynamic> showMarker({});
Future<dynamic> showDirections({});
```