/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  Any,
  FlutterPlugin,
  FlutterPluginBinding,
  MethodCall,
  MethodCallHandler,
  MethodChannel,
  MethodResult,
} from '@ohos/flutter_ohos';
import bundleManager from '@ohos.bundle.bundleManager';
import { MapModel } from './MapModel'
import { List } from '@kit.ArkTS';
import { MapType } from './MapType'
import { abilityAccessCtrl, common, Permissions, Want } from '@kit.AbilityKit';
import { BusinessError } from '@kit.BasicServicesKit';
import { BuildMapsUrl } from './BuildMapsUrl'
import { promptAction } from '@kit.ArkUI'

/** MapLauncherOhosPlugin **/
export default class MapLauncherOhosPlugin implements FlutterPlugin, MethodCallHandler {
  private channel: MethodChannel | null = null;
  private TAG: String = 'MapLauncherOhosPlugin';

  constructor() {
  }

  getUniqueClassName(): string {
    return "MapLauncherOhosPlugin"
  }

  onAttachedToEngine(binding: FlutterPluginBinding): void {
    this.channel = new MethodChannel(binding.getBinaryMessenger(), "map_launcher_ohos");
    this.channel.setMethodCallHandler(this)
  }

  onDetachedFromEngine(binding: FlutterPluginBinding): void {
    if (this.channel != null) {
      this.channel.setMethodCallHandler(null)
    }
  }

  getInstalledMaps(result: MethodResult) {
    //mapList
    let mapList = new List<MapModel>();
    mapList.add(new MapModel(MapType[0], 'GaoDe', '', 'amapuri://'));
    mapList.add(new MapModel(MapType[1], 'Petal', '', 'maps://'));
    console.error('MapLauncherOhosPlugin app installation:', JSON.stringify(mapList));
    result.success(mapList);
  }

  isMapAvailable(link: string): boolean {
    try {
      let data = bundleManager.canOpenLink(link);
      return data;
    } catch (error) {
      console.error('Error checking app installation:', error);
      showErrorToast(getContext(this));
      return false;
    }
  }

  onMethodCall(call: MethodCall, result: MethodResult): void {
    let methodType: String = call.method;
    if (methodType == 'getInstalledMaps') {
      this.getInstalledMaps(result);
    } else if (methodType == 'showMarker' || methodType == 'showDirections') {
      this.setDataAndToMap(call);
    } else if (methodType == 'isMapAvailable') {
      let mapType: string = call.argument('mapType');
      console.info('MapLauncherOhosPlugin call.argument mapType =', mapType);
      result.success(this.isMapAvailable(getLink(mapType)));
    } else {
      result.notImplemented();
    }
  }

  setDataAndToMap(call: MethodCall) {
    let mapType: string = call.argument('mapType');
    let url: string = call.argument('url');
    let latitude: string;
    let longitude: string;
    let title: string;
    let description: string;
    console.info('MapLauncherOhosPlugin call.argument mapType =', mapType + call.argument('latitude'));
    if (isMarker(call.method)) {
      latitude = call.argument('latitude');
      longitude = call.argument('longitude');
      title = call.argument('title');
      description = call.argument('description');
      this.startMap(mapType, title, latitude, longitude, url, '', '', '', '', true);
    } else {
      let destinationTitle: string = call.argument('destinationTitle');
      let destinationLatitude: string = call.argument('destinationLatitude');
      let destinationLongitude: string = call.argument('destinationLongitude');
      let originLatitude: string = call.argument('originLatitude');
      let originLongitude: string = call.argument('originLongitude');
      let originTitle: string = call.argument('originTitle');
      let directionsMode: string = call.argument('directionsMode');
      let waypoints: Any = call.argument('waypoints');
      console.info(this.TAG + 'waypoints: ' + JSON.stringify(waypoints) + ' directionsMode :' + directionsMode);
      this.startMap(mapType, originTitle, originLatitude, originLongitude, url, destinationTitle, destinationLatitude,
        destinationLongitude, directionsMode, false);
    }
  }

  startMap(mapType: string, title: string, latitude: string, longitude: string, url: string, dTitle: string,
    destinationLatitude: string, destinationLongitude: string, directionsMode: string, isMarker: boolean | true) {
    //Check if the application is installed
    if (!this.isMapAvailable(url)) {
      console.error(`${url}is not installed`);
      showErrorToast(getContext(this));
      return;
    }
    let flag = bundleManager.BundleFlag.GET_BUNDLE_INFO_WITH_SIGNATURE_INFO;
    let bundleInfo = bundleManager.getBundleInfoForSelfSync(flag);
    let appName = bundleInfo.name;
    let mapData: BuildMapsUrl =
      new BuildMapsUrl(mapType, title, latitude, longitude, url, dTitle, destinationLatitude,
        destinationLongitude, directionsMode, isMarker);
    let context = getContext(this) as common.UIAbilityContext;
    let want: Want = {
      uri: mapData.getUrl(mapType)
    };
    if (mapType == 'petal') {
      if (isMarker) {
        want = {
          bundleName: 'com.huawei.hmos.maps.app',
          uri: 'maps://locationInfo',
          parameters: {
            linkSource: appName,
            destinationLatitude: Number(latitude),
            destinationLongitude: Number(longitude),
            destinationName: title,
            vehicleType: mapData.getDirectionsMode(directionsMode, mapType)
          }
        }
      } else {
        want = {
          bundleName: 'com.huawei.hmos.maps.app',
          uri: 'maps://routes',
          parameters: {
            linkSource: appName,
            originLatitude: Number(latitude),
            originLongitude: Number(longitude),
            originName: title,
            destinationLatitude: Number(destinationLatitude),
            destinationLongitude: Number(destinationLongitude),
            destinationName: dTitle,
            vehicleType: mapData.getDirectionsMode(directionsMode, mapType)
          }
        }
      }
    }
    context.startAbility(want, (err: BusinessError) => {
      if (err.code) {
        console.error(`startAbility failed,code is ${err.code},message is ${err.message}`);
        return;
      }
      console.info('startAbility succeed');
    });
  }
}

function isMarker(arg0: string) {
  return arg0 == 'showMarker';
}

export class WayPotionsModel {
  title: String | null = null;
  latitude: String | null = null;
  longitude: String | null = null;

  constructor(title: String, latitude: String, longitude: String) {
    this.title = title;
    this.latitude = latitude;
    this.longitude = longitude;
  }
}

function showErrorToast(mContext: Context) {
  let resourceManager = getContext(mContext).resourceManager;
  let errorMessage: string | null = 'Application not installed';
  resourceManager.getStringValue($r('app.string.app_not_installed').id).then((value: string) => {
    errorMessage = value;
  }).catch((error: BusinessError) => {
    console.error("getStringValue promise error is " + error);
  });
  try {
    promptAction.showToast({
      message: errorMessage,
      duration: 2000
    });
  } catch (error) {
    let message = (error as BusinessError).message
    let code = (error as BusinessError).code
    console.error(`showToast args error code is ${code}, message is ${message}`);
  }
  ;
}

function getLink(mapType: string): string {
  switch (mapType) {
    case MapType[0]:
      return 'amapuri://';
    case MapType[1]:
      return 'maps://';
    default:
      return 'maps://';
  }
}